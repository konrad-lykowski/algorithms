package graph;

import java.util.*;


public class PrimSub {


    static class Edge {
        public Node x;
        public Node y;
        public int distanceToMe;

        public Edge(Node x, Node y) {
            this.x = x;
            this.y = y;
            this.distanceToMe = distanceToMe;
        }

        @Override
        public boolean equals(Object obj) {
            Edge egdge = (Edge) obj;
            if (egdge.x.id == this.x.id && egdge.y.id == this.y.id) {
                return true;
            }
            if (egdge.x.id == this.y.id && egdge.x.id == this.y.id) {
                return true;
            }
            return false;
        }

        @Override
        public int hashCode() {
            return Objects.hash(this.x.id, this.y.id) + Objects.hash(this.y.id, this.x.id);
        }
    }

    public static class MyComparator implements Comparator<Edge> {

        @Override
        public int compare(Edge o1, Edge o2) {
            Edge nodea = (Edge) o1;
            Edge nodeb = (Edge) o2;
            if (nodea.distanceToMe > nodeb.distanceToMe)
                return 1;
            else if (nodea.distanceToMe < nodeb.distanceToMe)
                return -1;
            else return 0;
        }
    }

    static class Node {
        int id;
        List<Node> children = new LinkedList<>();

        Node(int id) {
            this.id = id;
        }

        public boolean equals(Object obj) {
            Node egdge = (Node) obj;
            if (egdge.id == this.id) {
                return true;
            }
            return false;
        }

        @Override
        public int hashCode() {
            return Objects.hash(this.id);
        }
    }

    public static void main(String[] args) {
        Scanner in = new Scanner(System.in);
        HashMap<Integer, Node> nodes = new HashMap();
        HashMap<Edge, Integer> edgesLen = new HashMap();
        int edgesNum = in.nextInt();
        int vertexesNum = in.nextInt();
        for (int a0 = 0; a0 < vertexesNum; a0++) {
            Node x = new Node(in.nextInt());
            Node y = new Node(in.nextInt());
            int distance = in.nextInt();
            if (!nodes.containsKey(x.id)) {
                nodes.put(x.id, x);
            }
            if (!nodes.containsKey(y.id)) {
                nodes.put(y.id, y);
            }
            nodes.get(x.id).children.add(y);
            nodes.get(y.id).children.add(x);

            edgesLen.put(new Edge(x, y), distance);

        }

        traverse(in, nodes, edgesLen);

    }

    private static void traverse(Scanner in, HashMap<Integer, Node> nodes, HashMap<Edge, Integer> edgesLen) {
        PriorityQueue<Edge> edgePriorityQueue = new PriorityQueue(new MyComparator());
        Node start = nodes.get(in.nextInt());

        for (Node child : start.children) {
            Edge tmp = new Edge(start, child);
            tmp.distanceToMe = edgesLen.get(tmp);
            edgePriorityQueue.add(tmp);
        }
        HashSet<Integer> visited = new HashSet<>();
        visited.add(start.id);
        int sum = 0;
        while (!edgePriorityQueue.isEmpty()) {
            Edge edge = edgePriorityQueue.poll();
            if (visited.contains(edge.y.id) && visited.contains(edge.x.id)) {
                continue;
            }
            if (!visited.contains(edge.x.id)) {
                visited.add(edge.x.id);
                for (Node child2 : nodes.get(edge.x.id).children) {
                    Edge tmp = new Edge(edge.x, child2);
                    tmp.distanceToMe = edgesLen.get(tmp);
                    edgePriorityQueue.add(tmp);
                }
            }
            if (!visited.contains(edge.y.id)) {
                visited.add(edge.y.id);
                for (Node child2 : nodes.get(edge.y.id).children) {
                    Edge tmp = new Edge(child2, edge.y);
                    tmp.distanceToMe = edgesLen.get(tmp);
                    edgePriorityQueue.add(tmp);
                }
            }
            sum = sum + edge.distanceToMe;
        }
        System.out.println(sum);
    }


}
